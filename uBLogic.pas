{ .ini Structure
a= user_password
b= store_name
c= expiration_date
d= tab caption
e= tab url
}
unit uBLogic;
interface
uses
  Winapi.Windows, uWebBrowser, System.IniFiles, System.SysUtils, Classes,
  ExtCtrls,
  FireDAC.Comp.Client, Vcl.Dialogs, Vcl.ComCtrls, Data.db;
type
  TApp = class
  private
    FRemoveBar: Bool;
    FFirstRender: Bool;
    FWeb_Browser: TWeb_Browser;
    FAdmin_Password: String;
    FUser_Password: String;
    FExpirationDate: TDate;
    FStoreName: String;
    FIniFile: TIniFile;
    FIniSections: TStringList;
    FSectionsMemTable: TFDMemTable;
    usFormatSettings: TFormatSettings;
    FPowerBi_UserName: string;
    FPowerBi_UserPassword: string;
  published
    constructor Create(pAppIniName: String; pSectionMemTable: TFDMemTable);
    procedure SetUser_Password(const Value: String);
    procedure SetExpirationDate(const Value: TDate);
    procedure SetStoreName(const Value: String);
    procedure SetIniFile(const Value: TIniFile);
    procedure SetFirstRender(const Value: Bool);
    procedure SetWeb_Browser(const Value: TWeb_Browser);
    procedure SetRemoveBar(const Value: Bool);
    procedure SetAdmin_Password(const Value: String);
    procedure SetIniSections(const Value: TStringList);
    procedure SetSectionsMemTable(const Value: TFDMemTable);
    procedure ReadIniSettings;
    procedure SaveSettingsToIni;
    procedure ReadIniSections(pPageControl: TPageControl);
    procedure SaveSectionsToIni;
    procedure UpdateSettings(pPanel: TPanel);
    procedure UpdateTabSheets(pMTable: TFDMemTable; pPageControl: TPageControl);
    procedure CreateNewIniFile;
    function GetURLFromTabName(pTabName: String; pSectionMemTable: TFDMemTable): String;
    function GetAutoSizeFromTabName(pTabName: String; pSectionMemTable: TFDMemTable): boolean;
    function CheckIni(pAppIniName: String): Bool;
    function ExpiredDate: Bool;
    function EncryptString(const sBuffer : Widestring) : Widestring;
    function DecryptString(const sBuffer : Widestring) : Widestring;
    property RemoveBar: Bool read FRemoveBar write SetRemoveBar;
    property FirstRender: Bool read FFirstRender write SetFirstRender;
    property Web_Browser: TWeb_Browser read FWeb_Browser write SetWeb_Browser;
    property Admin_Password: String read FAdmin_Password
      write SetAdmin_Password;
    property User_Password: String read FUser_Password write SetUser_Password;
    property ExpirationDate: TDate read FExpirationDate write SetExpirationDate;
    property StoreName: String read FStoreName write SetStoreName;
    property IniFile: TIniFile read FIniFile write SetIniFile;
    property IniSections: TStringList read FIniSections write SetIniSections;
    property SectionsMemTable: TFDMemTable read FSectionsMemTable
      write SetSectionsMemTable;
    property PowerBi_UserName: string read FPowerBi_UserName write FPowerBi_UserName;
    property PowerBi_UserPassword: string read FPowerBi_UserPassword write FPowerBi_UserPassword;
  end;
implementation

{ TApp }

function TApp.ExpiredDate: Bool;
begin
  result:=false;
  if ExpirationDate<date then result:=true;
end;

function TApp.CheckIni(pAppIniName: String): Bool;
begin
  result := True;
  if not FileExists(pAppIniName) then
    result := False;
end;

constructor TApp.Create(pAppIniName: String; pSectionMemTable: TFDMemTable);
begin
  usFormatSettings := TFormatSettings.Create(1033);
  SetIniFile(TIniFile.Create(pAppIniName));
  IniSections := TStringList.Create;
  SetSectionsMemTable(pSectionMemTable);
  SetFirstRender(True);
  SetAdmin_Password('9999');
  PowerBi_UserName := '';
  PowerBi_UserPassword := '';
end;

function TApp.DecryptString(const sBuffer: Widestring): Widestring;
var
         i :Integer;
begin
         Result := sBuffer;
         for i:=1 to Length(sBuffer) do
             Result[i] := Chr(24 XOr Ord(sBuffer[i]));
end;
function TApp.EncryptString(const sBuffer: Widestring): Widestring;
var
         i : Integer;
begin
         Result := sBuffer;
         for i:=1 to Length(sBuffer) do
             Result[i] := Chr(24 XOr Ord(sBuffer[i]));
end;

function TApp.GetAutoSizeFromTabName(pTabName: String;
  pSectionMemTable: TFDMemTable): boolean;
begin
  result := false;
  with SectionsMemTable do
  begin
    Locate('tab_caption', pTabName, [loCaseInsensitive]);
    result := fieldbyname('tab_autosize').asInteger=1;
  end;
end;

function TApp.GetURLFromTabName(pTabName: String;
  pSectionMemTable: TFDMemTable): String;
begin
  result := '';
  with SectionsMemTable do
  begin
    Locate('tab_caption', pTabName, [loCaseInsensitive]);
    result := fieldbyname('tab_url').AsString;
  end;
end;

procedure TApp.ReadIniSections(pPageControl: TPageControl);
var
  i: Integer;
begin
  with IniFile do
  begin
    ReadSections(IniSections);
    IniSections.Delete(0);
    with SectionsMemTable do
    begin
      Active := True;
      for i := 0 to IniSections.Count - 1 do
      begin
        Append;
        fieldByname('tab_caption').AsString := DecryptString( ReadString(IniSections[i], 'd', ''));
        fieldByname('tab_url').AsString := DecryptString( ReadString(IniSections[i], 'e', ''));
        fieldByname('tab_autosize').AsString := DecryptString( ReadString(IniSections[i], 'f', ''));
        Post;
      end;
    end;
  end;
end;

procedure TApp.SaveSectionsToIni;
var
  i: Integer;
begin
  with IniFile do
  begin
    for i := 0 to IniSections.Count - 1 do
    begin
      EraseSection(IniSections[i]);
    end;
  end;
  IniSections.Clear;
  with SectionsMemTable do
  begin
    if not SectionsMemTable.Active then
       SectionsMemTable.Active := true;
    First;
    for i := 0 to RecordCount - 1 do
    begin
      with IniFile do
      begin
        IniSections.Add( 't' + IntToStr(i) );
      end;
      Next;
    end;
  end;
  with SectionsMemTable do
  begin
    First;
    for i := 0 to RecordCount - 1 do
    begin
      with IniFile do
      begin
        WriteString(IniSections[i], 'd', EncryptString(  fieldByName('tab_caption').AsString) );
        WriteString(IniSections[i], 'e', EncryptString(  fieldByName('tab_url').AsString) );
        WriteString(IniSections[i], 'f', EncryptString(  fieldByName('tab_autosize').AsString) );
      end;
      Next;
    end;
  end;
end;

procedure TApp.UpdateTabSheets(pMTable: TFDMemTable;
  pPageControl: TPageControl);
var
  i: Integer;
begin
  for i := 0 to pPageControl.PageCount - 1 do
    pPageControl.Pages[0].Free;
  with pMTable do
  begin
    First;
    for i := 0 to RecordCount - 1 do
    begin
      with TTabSheet.Create(pPageControl) do
      begin
        PageControl := pPageControl;
        Caption := Fields[0].AsString;
        Name := Fields[0].AsString;
      end;
      Next;
    end;
    pPageControl.ActivePageIndex := 0;
    if pPageControl.PageCount = 1 then
      pPageControl.Hide
    else
      pPageControl.Show;
  end;
end;

procedure TApp.ReadIniSettings;
begin
  with IniFile do
  begin
    SetUser_Password(DecryptString(ReadString('s', 'a', '-1.-')));
    SetStoreName(DecryptString(ReadString('s', 'b', '-1.-')));
    SetExpirationDate(StrToDate(DecryptString( ReadString('s', 'c','01/01/1980')), usFormatSettings));
    PowerBi_UserName := DecryptString(IniFile.ReadString('s', 'g', '-1.-'));
    PowerBi_UserPassword := DecryptString(IniFile.ReadString('s', 'h', '-1.-'));
  end;
end;

procedure TApp.SaveSettingsToIni;
begin
  with IniFile do
  begin
    WriteString('s', 'a', EncryptString(User_Password));
    WriteString('s', 'b', EncryptString(StoreName));
    WriteString('s', 'c', EncryptString(DateToStr(ExpirationDate, usFormatSettings)));
    WriteString('s', 'g', EncryptString(PowerBi_UserName));
    WriteString('s', 'h', EncryptString(PowerBi_UserPassword));
  end;
end;

procedure TApp.SetAdmin_Password(const Value: String);
begin
  FAdmin_Password := Value;
end;

procedure TApp.SetExpirationDate(const Value: TDate);
begin
  FExpirationDate := Value;
end;

procedure TApp.SetFirstRender(const Value: Bool);
begin
  FFirstRender := Value;
end;

procedure TApp.SetIniFile(const Value: TIniFile);
begin
  FIniFile := Value;
end;

procedure TApp.SetIniSections(const Value: TStringList);
begin
  FIniSections := Value;
end;

procedure TApp.SetRemoveBar(const Value: Bool);
begin
  FRemoveBar := Value;
end;

procedure TApp.SetSectionsMemTable(const Value: TFDMemTable);
begin
  FSectionsMemTable := Value;
end;

procedure TApp.SetStoreName(const Value: String);
begin
  FStoreName := Value;
end;

procedure TApp.SetUser_Password(const Value: String);
begin
  FUser_Password := Value;
end;

procedure TApp.SetWeb_Browser(const Value: TWeb_Browser);
begin
  FWeb_Browser := Value;
end;

procedure TApp.UpdateSettings(pPanel: TPanel);
begin
  pPanel.Caption := 'CompuDime Multistore Dashboard - ' + StoreName;
end;

procedure TApp.CreateNewIniFile;
begin
if( Assigned(FIniFile)) then
  begin
    SetUser_Password('');
    SetStoreName('Store Name');
    SetExpirationDate(now());
    PowerBi_UserName := '';
    PowerBi_UserPassword := '';
    SaveSettingsToIni;
    SaveSectionsToIni;
  end;
end;
end.
