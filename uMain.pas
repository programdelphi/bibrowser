unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Edge,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.OleCtrls, SHDocVw, MSHTML, uBLogic,
  uWebBrowser, Vcl.Menus, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.Buttons, Vcl.StdCtrls, webview2,
  Winapi.ActiveX, Vcl.Imaging.pngimage  ;

Type
 THackBrowser = Class(TWebBrowser);

type
  TfMain = class(TForm)
    pHeading: TPanel;
    pBottom: TPanel;
    pcURL: TPageControl;
    sbExit: TSpeedButton;
    lblver: TLabel;
    Image1: TImage;
    Timer1: TTimer;
    lblVersion: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure pcURLChange(Sender: TObject);
    procedure sbExitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure WebBrowser1NavigationCompleted(Sender: TCustomEdgeBrowser;
      IsSuccess: Boolean; WebErrorStatus: TOleEnum);
    procedure EdgeBrowser1NavigationStarting(Sender: TCustomEdgeBrowser;
      Args: TNavigationStartingEventArgs);
    procedure EdgeBrowser1ContentLoading(Sender: TCustomEdgeBrowser;
      IsErrorPage: Boolean; NavigationID: TUInt64);
    procedure EdgeBrowser1FrameNavigationStarting(Sender: TCustomEdgeBrowser;
      Args: TNavigationStartingEventArgs);
    procedure EdgeBrowser1FrameNavigationCompleted(Sender: TCustomEdgeBrowser;
      IsSuccess: Boolean; WebErrorStatus: TOleEnum);
    procedure FormDestroy(Sender: TObject);
    procedure EdgeBrowser1WebMessageReceived(Sender: TCustomEdgeBrowser; Args: TWebMessageReceivedEventArgs);
    procedure EdgeBrowser1NewWindowRequested(Sender: TCustomEdgeBrowser; Args: TNewWindowRequestedEventArgs);
  private
    { Private declarations }
    FrameLoading :boolean;
    OWeb_Browser: TWeb_Browser;
    WebBrowser1 :TEdgeBrowser;
    EMailErr, PasswordErr: Boolean;
    LogAll: Boolean;
    procedure GetAppLogingSW;
    procedure AddToLog(LogLevel, LogMessage: string);
    procedure SendLoginToMSOAuth(ExtraWait: Boolean = false);
  public
    { Public declarations }
    App: TApp;
    LoggingFileSW: TStreamWriter;
  end;

var
  fMain: TfMain;

implementation

{$R *.dfm}

uses
  System.IOUtils, System.StrUtils, uSetup, uDm, uAdminPassWord, uPassWord;


procedure TfMain.AddToLog(LogLevel, LogMessage: string);
begin
    LoggingFileSW.WriteLine(FormatDateTime('yyyy-mm-dd hh:nn:ss.zz', Now())+ #9 + LogLevel + #9  +LogMessage);
end;

procedure TfMain.EdgeBrowser1ContentLoading(Sender: TCustomEdgeBrowser;
  IsErrorPage: Boolean; NavigationID: TUInt64);
begin
//  with App do
//  begin
//    WebBrowser1.ExecuteScript('document.querySelector("div.logo a").href = "https://i.ibb.co/jgVF0j6/bi.png";');
//    WebBrowser1.ExecuteScript('document.querySelector("div.logo span").innerHTML = "<img style=\"height:12px\" src=\"https://i.ibb.co/jgVF0j6/bi.png\">&nbsp;'+App.Web_Browser.NewLinkText +'";');
//    WebBrowser1.ExecuteScript('document.querySelector("span.socialSharing").style = "display: none;"');
//  end;
  //Webbrowser1.DefaultContextMenusEnabled := False;
 // Webbrowser1.DevToolsEnabled := False;
end;

procedure TfMain.EdgeBrowser1FrameNavigationCompleted(
  Sender: TCustomEdgeBrowser; IsSuccess: Boolean; WebErrorStatus: TOleEnum);
begin
   FrameLoading := false;
   //TTimer(Sender).Enabled := true;
end;

procedure TfMain.EdgeBrowser1FrameNavigationStarting(Sender: TCustomEdgeBrowser;
  Args: TNavigationStartingEventArgs);
begin
   FrameLoading := true;
end;

procedure TfMain.EdgeBrowser1NavigationStarting(Sender: TCustomEdgeBrowser;
  Args: TNavigationStartingEventArgs);
begin
  FrameLoading := false;
//  with App do
//  begin
//    WebBrowser1.ExecuteScript('document.querySelector("div.logo a").href = "https://i.ibb.co/jgVF0j6/bi.png";');
//    WebBrowser1.ExecuteScript('document.querySelector("div.logo span").innerHTML = "<img style=\"height:12px\" src=\"https://i.ibb.co/jgVF0j6/bi.png\">&nbsp;'+App.Web_Browser.NewLinkText +'";');
//    WebBrowser1.ExecuteScript('document.querySelector("span.socialSharing").style = "display: none;"');
//  end;

 // Webbrowser1.DefaultContextMenusEnabled := False;
  //Webbrowser1.DevToolsEnabled := False;
  {$IFNDEF TEST}
  Webbrowser1.Hide; // test
  {$ENDIF}
end;

procedure TfMain.EdgeBrowser1WebMessageReceived(Sender: TCustomEdgeBrowser; Args: TWebMessageReceivedEventArgs);
var
  wchWebmsg: PWideChar;
  Webmsg, MsgTypeCode, MsgType, LogMsg: string;
  posMsg: integer;
begin
  var msg := Args as ICoreWebView2WebMessageReceivedEventArgs;

  msg.TryGetWebMessageAsString(wchWebmsg);
  Webmsg := wchWebmsg;
  posMsg := Pos(':', wchWebmsg);
  if PosMsg > 0 then
  begin
    MsgTypeCode := Copy(Webmsg, 1, posMsg - 1);
    if MsgTypeCode = 'i' then
      MsgType := 'Info'
    else if MsgTypeCode = 'w' then
      MsgType := 'Warning'
    else if MsgTypeCode = 'e' then
      MsgType := 'Error'
    else if MsgTypeCode = 'u' then
      MsgType := 'Error'
    else if MsgTypeCode = 'p' then
      MsgType := 'Error';

    LogMsg := Copy(Webmsg, posMsg + 1, Length(Webmsg));

    if (MsgTypeCode = 'l') then // normal login
    begin
      MsgType := 'Info';
     if LogAll then
       AddToLog(MsgType, LogMsg);

      if timer1.Enabled then
        timer1.Enabled := False;

      SendLoginToMSOAuth(false);

      if not Sender.Visible then
        Sender.Visible := True;
     {$IFNDEF TEST}
      Sender.Align :=  alBottom;
      Sender.Height := 1;
     {$ENDIF}
      webBrowser1.SetFocus;
      Application.ProcessMessages;
    end
    else
    if (MsgTypeCode = 'o') then // Other account is selected
    begin
      MsgType := 'Info';
      if LogAll then
       AddToLog(MsgType, LogMsg);

      if timer1.Enabled then
        timer1.Enabled := False;

      SendLoginToMSOAuth(false);

      if not Sender.Visible then
        Sender.Visible := True;
     {$IFNDEF TEST}
      Sender.Align :=  alBottom;
      Sender.Height := 1;
     {$ENDIF}
      webBrowser1.SetFocus;
      Application.ProcessMessages;
    end
    else
      AddToLog(MsgType, LogMsg);

    if (MsgTypeCode = 'u') then
    begin
      EMailErr := True;
      if timer1.Enabled then
        timer1.Enabled := False;
     {$IFNDEF TEST}
      if Sender.Visible then
        Sender.Visible := False;
     {$ENDIF}
    end
    else if (MsgTypeCode = 'p') then
    begin
      PasswordErr := True;
      if timer1.Enabled then
        timer1.Enabled := False;
     {$IFNDEF TEST}
      if Sender.Visible then
        Sender.Visible := False;
     {$ENDIF}
    end;
  end
  else
    AddToLog('Info', Webmsg);
end;

procedure TfMain.EdgeBrowser1NewWindowRequested(Sender: TCustomEdgeBrowser; Args: TNewWindowRequestedEventArgs);
begin
  //* Open link in current WebView2:
  Args.ArgsInterface.Set_NewWindow(Sender.DefaultInterface);
end;

procedure TfMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TfMain.FormCreate(Sender: TObject);
begin
  {$IFDEF TEST}
  LogAll := True;
  {$ENDIF}

  if (ParamCount >= 1) and (ParamStr(1) = 'log-all') then
    LogAll := True;

  FrameLoading := false;
  Image1.Align := alClient;

  GetAppLogingSW;

  if not((ParamCount >= 1) and (ParamStr(1) = 'setup')) then
  begin
    WebBrowser1 := TEdgeBrowser.Create(Self);
    TWinControl(WebBrowser1).Parent := Self;
    WebBrowser1.Align := alClient;
    WebBrowser1.Visible := true;
    WebBrowser1.OnNavigationCompleted  := WebBrowser1NavigationCompleted;
    WebBrowser1.OnNavigationStarting  := EdgeBrowser1NavigationStarting;
    WebBrowser1.OnContentLoading := EdgeBrowser1ContentLoading;
    WebBrowser1.OnFrameNavigationCompleted:= EdgeBrowser1FrameNavigationCompleted;
    WebBrowser1.OnFrameNavigationStarting:= EdgeBrowser1FrameNavigationStarting;
    WebBrowser1.OnWebMessageReceived := EdgeBrowser1WebMessageReceived;
    WebBrowser1.OnNewWindowRequested := EdgeBrowser1NewWindowRequested;
  end;
  App := TApp.Create(ChangeFileExt(Application.ExeName, '.ini'), Dm.mtURL);
  OWeb_Browser := TWeb_Browser.Create;
  App.SetWeb_Browser(OWeb_Browser);
  if not App.CheckIni(ChangeFileExt(Application.ExeName, '.ini')) then
  begin
    if (ParamCount >= 1) and (ParamStr(1) = 'setup') then
      App.CreateNewIniFile
    else
    begin
      ShowMessage('Error: No Setup .ini file detected.');
      Application.Terminate;
    end;
  end;
  with App do
  begin
    ReadIniSettings;
    ReadIniSections(pcURL);
    UpdateSettings(pHeading);
    UpdateTabSheets(Dm.mtURL, fMain.pcURL);
  //  SetRemoveBar(True);  //yona
    Web_Browser.NewLinkText := 'MY BI';
    Web_Browser.SetURL(GetURLFromTabName(pcURL.ActivePage.Caption, Dm.mtURL));
    Web_Browser.SetAutoSize(GetAutoSizeFromTabName(pcURL.ActivePage.Caption, Dm.mtURL));
  end;
end;

procedure TfMain.FormDestroy(Sender: TObject);
begin
  if Assigned(LoggingFileSW) then
  begin
    LoggingFileSW.Close;
    if LoggingFileSW.BaseStream <> nil then
      LoggingFileSW.BaseStream.Free;
    LoggingFileSW.Free;
  end;
end;

procedure TfMain.FormShow(Sender: TObject);
begin
  if ParamCount >= 1 then
    if ParamStr(1) = 'setup' then
    begin
      Application.CreateForm(TfAdminPassword, fAdminPassword);
      Application.CreateForm(TfSetup, fSetup);
      fAdminPassword.ShowModal;
    end;
  if App.ExpiredDate then
  begin
    ShowMessage('Error: Contact System Administrator.');
    Application.Terminate;
    exit;
  end;
  if App.User_Password <> '' then
  begin
    Application.CreateForm(TfPasswordDlg, fPasswordDlg);
    if (fPasswordDlg.ShowModal <> mrOk) then
      Application.Terminate;
  end;
  if not((ParamCount >= 1) and (ParamStr(1) = 'setup')) then
    WebBrowser1.Navigate(App.Web_Browser.URL);
  //EdgeBrowser.Navigate(App.Web_Browser.URL)   ;
end;

procedure TfMain.pcURLChange(Sender: TObject);
begin
  with App do
  begin
    Web_Browser.SetURL(GetURLFromTabName(pcURL.ActivePage.Caption, Dm.mtURL));
    Web_Browser.SetAutoSize(GetAutoSizeFromTabName(pcURL.ActivePage.Caption, Dm.mtURL));
    // WebBrowser1.Navigate2(Web_Browser.URL);
     WebBrowser1.Navigate(Web_Browser.URL);
      SetFirstRender(True);
  end;
end;

procedure TfMain.sbExitClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfMain.SendLoginToMSOAuth(ExtraWait: Boolean = false);
var
  ExtraWaitTime: integer;
begin
  if ExtraWait then
    ExtraWaitTime := 200
  else
    ExtraWaitTime := 0;

  with App do
  begin
    WebBrowser1.ExecuteScript(
        'setTimeout(() => {'#13#10+
        ' console.log("username check!");'#13#10+
        ' let username = document.querySelector("#i0116");'#13#10+
        ' if (username !== null) {'#13#10+
        '  username.focus();'#13#10+
        '  username.value = "'+PowerBi_UserName+'";'#13#10+
        '  console.log("Username is assigned! (2)");'#13#10+
        IfThen(LogAll,
        '  window.chrome.webview.postMessage("Username is assigned! (2)");'#13#10, '')+
        ' }'#13#10+
        '}, ' + IntToStr(550 + ExtraWaitTime) + ');'
       );

      WebBrowser1.ExecuteScript(
        'setTimeout(() => {'#13#10+
        ' console.log("username check ad submit!");'#13#10+
        ' let username = document.querySelector("#i0116");'#13#10+
        ' if (username !== null) {'#13#10+
        '  let btnPwdSubmit = document.querySelector("#idSIButton9");'#13#10+
        '  if (btnPwdSubmit !== null) {'#13#10+
        '   btnPwdSubmit.focus();'#13#10+
        '   console.log("Send button is clicked!");'#13#10+
        IfThen(LogAll,
        '   window.chrome.webview.postMessage("User name/password form is submitted! (2)");'#13#10, '') +
        '   btnPwdSubmit.click();'#13#10+
        '   setTimeout(() => {'#13#10+
        '    let usernameErr = document.querySelector("#usernameError");'#13#10+
        '    if ((usernameErr !== null) && (usernameErr.textContent != "")) {'#13#10+
        '     console.log("e:"+usernameErr.textContent);'#13#10+
        '     window.chrome.webview.postMessage("u:"+usernameErr.textContent);'#13#10+
        '    }'#13#10+
        '    let passwordErr = document.querySelector("#passwordError");'#13#10+
        '    if ((passwordErr !== null) && (passwordErr.textContent != "")) {'#13#10+
        '     console.log("p:"+passwordErr.textContent);'#13#10+
        '     window.chrome.webview.postMessage("p:"+passwordErr.textContent);'#13#10+
        '    }'#13#10+
        '   }, 50);'#13#10+
        '  }'#13#10+
        ' }'#13#10+
        '}, ' + IntToStr(600 + ExtraWaitTime) + ');');

    WebBrowser1.ExecuteScript(
        'setTimeout(() => {'#13#10+
        ' console.log("password check!");'#13#10+
        ' let pwd = document.querySelector("#i0118");'#13#10+
        ' if (pwd !== null) {'#13#10+
        '  pwd.focus();'#13#10+
        '  pwd.value = "'+PowerBi_UserPassword+'";'#13#10+
        '  console.log("Password is assigned (2)!");'#13#10+
        IfThen(LogAll,
        '  window.chrome.webview.postMessage("Password is assigned! (2)");'#13#10, '') +
        ' }'#13#10+
        '}, ' + IntToStr(1500 + ExtraWaitTime) + ');'
       );

      WebBrowser1.ExecuteScript(
        'setTimeout(() => {'#13#10+
        ' console.log("submit check!");'#13#10+
        '  let btnPwdSubmit = document.querySelector("#idSIButton9");'#13#10+
        '  if ((btnPwdSubmit !== null) && (btnPwdSubmit.style["display"] !== "none")) {'#13#10+
        '   btnPwdSubmit.focus();'#13#10+
        '   console.log("Send button is clicked!");'#13#10+
        IfThen(LogAll,
        '   window.chrome.webview.postMessage("User name/password form is submitted! (2)");'#13#10, '') +
        '   btnPwdSubmit.click();'#13#10+
        '   setTimeout(() => {'#13#10+
        '    let usernameErr = document.querySelector("#usernameError");'#13#10+
        '    if ((usernameErr !== null) && (usernameErr.textContent != "")) {'#13#10+
        '     console.log("e:"+usernameErr.textContent);'#13#10+
        '     window.chrome.webview.postMessage("u:"+usernameErr.textContent);'#13#10+
        '    }'#13#10+
        '    let passwordErr = document.querySelector("#passwordError");'#13#10+
        '    if ((passwordErr !== null) && (passwordErr.textContent != "")) {'#13#10+
        '     console.log("p:"+passwordErr.textContent);'#13#10+
        '     window.chrome.webview.postMessage("p:"+passwordErr.textContent);'#13#10+
        '    }'#13#10+
        '   }, 50);'#13#10+
        '  }'#13#10+
        '}, ' + IntToStr(1800 + ExtraWaitTime) + ');');

    if ExtraWait then
    begin
      WebBrowser1.ExecuteScript(
        'setTimeout(() => {'#13#10+
        ' console.log("last submit!");'#13#10+
        ' let btnPwdSubmit = document.querySelector("#idSIButton9");'#13#10+
        ' if (btnPwdSubmit !== null) {'#13#10+
        '  btnPwdSubmit.focus();'#13#10+
        '  console.log("Send button is clicked for last time!");'#13#10+
        {$IFDEF TEST}
        '  window.chrome.webview.postMessage("Password form is submitted! (3)");'#13#10+
        {$ENDIF}
        '  btnPwdSubmit.click();'#13#10+
        '  setTimeout(() => {'#13#10+
        '   let usernameErr = document.querySelector("#usernameError");'#13#10+
        '   if ((usernameErr !== null) && (usernameErr.textContent != "")) {'#13#10+
        '    console.log("e:"+usernameErr.textContent);'#13#10+
        '    window.chrome.webview.postMessage("u:"+usernameErr.textContent);'#13#10+
        '   }'#13#10+
        '   let passwordErr = document.querySelector("#passwordError");'#13#10+
        '   if ((passwordErr !== null) && (passwordErr.textContent != "")) {'#13#10+
        '    console.log("p:"+passwordErr.textContent);'#13#10+
        '    window.chrome.webview.postMessage("p:"+passwordErr.textContent);'#13#10+
        '   }'#13#10+
        '  }, 50);'#13#10+
        ' }'#13#10+
        '}, ' + IntToStr(1700 + ExtraWaitTime) + ');');
    end;
  end;
end;

procedure TfMain.Timer1Timer(Sender: TObject);
begin
  TTimer(Sender).Enabled := False;

  //if not FrameLoading then
  //begin
    WebBrowser1.show;
    WebBrowser1.Align := alClient;
    webbrowser1.BringToFront;
    webBrowser1.Visible := true;
    application.ProcessMessages;
    webBrowser1.SetFocus;
  //end;
end;

procedure TfMain.GetAppLogingSW;
var
  LogDir, FileName: string;
  LoggingFileStream: TFileStream;
begin
  LogDir := ExtractFilePath(Application.ExeName) + 'log';

  if not DirectoryExists(LogDir) then
    ForceDirectories(LogDir);

  FileName := LogDir + '\' + TPath.GetFileNameWithoutExtension(Application.ExeName) + '.log';

  if  not FileExists(FileName) then
    LoggingFileStream :=
      TFileStream.Create(FileName, fmCreate or fmOpenReadWrite or fmShareDenyNone)
  else
    LoggingFileStream :=
      TFileStream.Create(FileName, fmOpenReadWrite or fmShareDenyNone);
  LoggingFileStream.Position := LoggingFileStream.Size; // append to stream;
  LoggingFileSW := TStreamWriter.Create(LoggingFileStream, TEncoding.UTF8);
end;

procedure TfMain.WebBrowser1NavigationCompleted(Sender: TCustomEdgeBrowser;
  IsSuccess: Boolean; WebErrorStatus: TOleEnum);
begin
  with App do
  begin
    WebBrowser1.ExecuteScript('document.querySelector("div.logo a").href = "https://i.ibb.co/jgVF0j6/bi.png";');
    WebBrowser1.ExecuteScript('document.querySelector("div.logo span").innerHTML = "<img style=\"height:12px\" src=\"https://i.ibb.co/jgVF0j6/bi.png\">&nbsp;'+App.Web_Browser.NewLinkText +'";');
    WebBrowser1.ExecuteScript('document.querySelector("span.socialSharing").style = "display: none;"');

    if Sender.LocationURL.StartsWith('https://app.powerbi.com') then
    begin
      WebBrowser1.ExecuteScript(
        'document.querySelector(".powerBILogoImg").src = "https://i.ibb.co/jgVF0j6/bi.png";');

      WebBrowser1.ExecuteScript(
        'setTimeout(() => {'#13#10+
        ' let pbilogo = document.querySelector("#pbi-svg-loading img");'#13#10+
        ' if (pbilogo !== null) {'#13#10+
        '  pbilogo.src = "https://i.ibb.co/jgVF0j6/bi.png";'#13#10+
        //'  console.logo("Power Bi logo is found!");'#13#10+
        //'  window.chrome.webview.postMessage("Power Bi logo is found!");'#13#10+
        ' }'#13#10+
        //' else {'#13#10+
        //'  console.logo("Power Bi logo is not found!");'#13#10+
        //'  window.chrome.webview.postMessage("Power Bi logo is not found!");'#13#10+
        //' }'#13#10+
        '}, 0);');
    end;

    if Sender.LocationURL.StartsWith('https://app.powerbi.com/singleSignOn', true) then
    begin
      EMailErr := False;
      PasswordErr := False;
      WebBrowser1.ExecuteScript(
        'let email = document.querySelector("#email");'#13#10+
        'if (email !== null) {'#13#10+
        ' email.value = "'+PowerBi_UserName+'";'#13#10+
        ' console.log("Username is assigned!");'#13#10+
        {$IFDEF TEST}
        ' window.chrome.webview.postMessage("Username is assigned! (1)");'#13#10+
        {$ENDIF}
        ' let submitBtn = document.querySelector("#submitBtn");'#13#10+
        ' if (submitBtn !== null) {'#13#10+
        {$IFDEF TEST}
        '  window.chrome.webview.postMessage("User name form is submitted! (1)");'#13#10+
        {$ENDIF}
        '  submitBtn.click();'#13#10+
        '  setTimeout(() => {'#13#10+
        '   let emailInputErr = document.querySelector(".emailInputError");'#13#10+
        '   if ((emailInputErr !== null) && (emailInputErr.textContent != "")) {'+
        '    console.log("u:"+emailInputErr.textContent);'#13#10+
        '    window.chrome.webview.postMessage("u:"+emailInputErr.textContent);'#13#10+
        '   }'#13#10+
        '  }, 5);'#13#10+
        ' }'#13#10+
        ' else {'#13#10+
        '  console.log("u:submit button for e-mail form could not be found!");'#13#10+
        '  window.chrome.webview.postMessage("u:submit button for e-mail form could not be found!");'#13#10+
        ' }'#13#10+
        '}'#13#10+
        'else {'#13#10+
        ' console.log("u:e-mail textbox could not be found!");'#13#10+
        ' window.chrome.webview.postMessage("u:e-mail textbox could not be found!");'#13#10+
        '}');
    end
    else if Sender.LocationURL.StartsWith('https://signup.microsoft.com/create-account/signup', true) then
    begin
      EMailErr := True;
      AddToLog('Error', Format('No Power BI account exists for the email address: %s', [PowerBi_UserName]));
      {$IFNDEF TEST}
      Webbrowser1.Hide;
      {$ENDIF}
    end
    else if Sender.LocationURL.StartsWith('https://login.microsoftonline.com/common/oauth2/v2.0/authorize') or
            Sender.LocationURL.StartsWith('https://login.microsoftonline.com/common/oauth2/authorize') or
            Sender.LocationURL.StartsWith('https://login.microsoftonline.com/' + Web_Browser.ctid + '/oauth2/v2.0/authorize') or
            Sender.LocationURL.StartsWith('https://login.microsoftonline.com/' + Web_Browser.ctid + '/oauth2/authorize') then
    begin
      EMailErr := False;
      PasswordErr := False;

      application.ProcessMessages;

      WebBrowser1.ExecuteScript(
        'setTimeout(() => {'#13#10+
        ' let logo = document.querySelector("img.logo");'#13#10+
        ' if (logo !== null) {'#13#10+
        '  logo.style = "display: none;";'#13#10+
        ' }'#13#10+
        ' let otherTile = document.querySelector("#otherTile");'#13#10+ // select other account if account selection is asked
        ' if (otherTile !== null) {'#13#10+
        '  console.log("Use other account is clicked!");'#13#10+
        '  window.chrome.webview.postMessage("o:Use other account is clicked!");'#13#10+
        '  otherTile.click();'#13#10+
        ' }'#13#10+
        ' else {'#13#10+
        '  window.chrome.webview.postMessage("l:normal login will be done!");'#13#10+
        ' }'#13#10+
        '}, 350);');

      //SendLoginToMSOAuth(false);

      webBrowser1.Visible := true;
     {$IFNDEF TEST}
      webBrowser1.Align := alBottom;
      webBrowser1.Height := 1;
     {$ENDIF}
      webBrowser1.SetFocus;
      application.ProcessMessages;
    end
    else if self.WebBrowser1.LocationURL.StartsWith('https://login.microsoftonline.com/common/login') or
            self.WebBrowser1.LocationURL.StartsWith('https://login.microsoftonline.com/' + Web_Browser.ctid + '/login') then
    begin
      EMailErr := False;
      PasswordErr := False;

      Application.ProcessMessages;
      WebBrowser1.ExecuteScript(
        'setTimeout(() => {'#13#10+
        ' let logo = document.querySelector("img.logo");'#13#10+
        ' if (logo !== null) {'#13#10+
        '  logo.style = "display: none;";'#13#10+
        ' }'#13#10+
        ' let passwordErr = document.querySelector("#passwordError");'#13#10+
        ' if ((passwordErr !== null) && (passwordErr.textContent != "")) {'#13#10+
        '  console.log("p:"+passwordErr.textContent);'#13#10+
        '  window.chrome.webview.postMessage("p:"+passwordErr.textContent);'#13#10+
        ' }'#13#10+
        ' else {'#13#10+
        '  setTimeout(() => {'#13#10+
        '   let Btn_Back = document.querySelector("#idBtn_Back");'#13#10+  // no to keep seesion open
        '   if (Btn_Back !== null) {'#13#10+
        '    Btn_Back.click();'#13#10+
        '   }'#13#10+
        '  }, 50);'#13#10+
        ' }'#13#10+
        '}, 50);');
    end
    else if self.WebBrowser1.LocationURL.StartsWith('https://login.microsoftonline.com/common/oauth2/logout') or
            self.WebBrowser1.LocationURL.StartsWith('https://login.microsoftonline.com/' + Web_Browser.ctid + '/oauth2/logout') then
    begin
      WebBrowser1.ExecuteScript(
        'setTimeout(() => {'#13#10+
        ' let banerlogo = document.querySelector("img.banner-logo");'#13#10+
        ' if (banerlogo !== null) {'#13#10+
        '  banerlogo.style = "display: none;";'#13#10+
        ' }'#13#10+
        ' let logo = document.querySelector("img.logo");'#13#10+
        ' if (logo !== null) {'#13#10+
        '  logo.style = "display: none;";'#13#10+
        ' }'#13#10+
        '}, 100);');
      if not EMailErr and not PasswordErr then
        Timer1.Enabled := true;
      Application.ProcessMessages;
    end
    else if self.WebBrowser1.LocationURL.StartsWith('https://app.powerbi.com/reportEmbed') then
    begin
      WebBrowser1.ExecuteScript(
        'setTimeout(() => {'#13#10+
        ' console.log("signin button is being checked!");'#13#10+
        IfThen(LogAll,
        ' window.chrome.webview.postMessage("signin button is being checked!");'#13#10, '')+
        ' let signinbtn = document.querySelector("#promptForLogin > button");'#13#10+  // no to keep seesion open
        ' if (signinbtn !== null) {'#13#10+
        '  signinbtn.click();'#13#10+
        ' }'#13#10+
        '}, 0);');

      WebBrowser1.ExecuteScript(
        'document.addEventListener("mousedown", documentmousedown, false);'#13#10+
        ''#13#10+
        'function documentmousedown(e) {'#13#10+
        '    console.log(`Mouse down event occured on document. Target tag: ${e.target.tagName}, id: ${e.target.id}, class: ${e.target.className}`);'#13#10+
        IfThen(LogAll,
        '    window.chrome.webview.postMessage(`Mouse down event occured on document. Target tag; ${e.target.tagName}, id; ${e.target.id}, class; ${e.target.className}`);'#13#10, '')+
        '    if (e.target.className === "glyphicon pbi-glyph-more glyph-small") {'#13#10+
        '        let divmenu = document.querySelector("visual-header-item-container > div > button");'#13#10+
        '        if (divmenu !== null) {'#13#10+
        '            console.log("divmenu is found.");'#13#10+
        IfThen(LogAll,
        '            window.chrome.webview.postMessage("divmenu is found.");'#13#10, '')+
        '            divmenu.addEventListener("click", otheroptionsmenuclick, false);'#13#10+
        '        }'#13#10+
        '    }'#13#10+
        '}'#13#10+
        ''#13#10+
        'function otheroptionsmenuclick() {'#13#10+
        '    console.log("Other options click runs!");'#13#10+
        '    let btn1o = document.querySelector(".cdk-overlay-pane > ng-component > pbi-menu > button:nth-child(1)");'+
        '    if (btn1o !== null) {'#13#10+
        '        console.log("<Export data> is found.");'#13#10+
        IfThen(LogAll,
        '        window.chrome.webview.postMessage("<Export data> is found.");'#13#10, '')+
        '        btn1o.addEventListener("click", exportreportclick, false);'#13#10+
	      '    }'#13#10+
        // Hide show as a table menu item
        '    let btn2o = document.querySelector(".cdk-overlay-pane > ng-component > pbi-menu > button:nth-child(2)");'#13#10+
        '    if (btn2o !== null) {'#13#10+
        '        console.log("<Show as a table> is found.");'#13#10+
        IfThen(LogAll,
        '        window.chrome.webview.postMessage("<Show as a table> is found.");'#13#10, '')+
        '        btn2o.style.display = "none";'#13#10+
        '    }'#13#10+
        // Hide get insights menu item
        '    let btn4o = document.querySelector(".cdk-overlay-pane > ng-component > pbi-menu > button:nth-child(4)");'#13#10+
        '    if (btn4o !== null) {'#13#10+
        '        console.log("<Get insights> is found.");'#13#10+
        IfThen(LogAll,
        '        window.chrome.webview.postMessage("<Get insights> is found.");'#13#10, '')+
        '        btn4o.style.display = "none";'#13#10+
        '    }'#13#10+
        '}'#13#10+
        #13#10+
        'function exportreportclick() {'#13#10+
        ' let runCount = 0;'+
        ' let interval = 20;'+
        ' setTimeout(check_aLearnMore, interval);'#13#10+
        ' function check_aLearnMore(){ '#13#10+
        '    ++runCount;'#13#10+
        '    aLearnMore = document.querySelector("mat-dialog-container > export-data-dialog > mat-dialog-content > div.fluentTheme-md-reg.exportDescription > p > a");'#13#10+
        '    if (aLearnMore !== null) {'#13#10+
        '        console.log(`Learn more link is found. (Passed time: ${runCount * interval})`);'#13#10+
        IfThen(LogAll,
        '        window.chrome.webview.postMessage(`Learn more link is found. (Passed time is ${runCount * interval} ms.)`);'#13#10, '')+
        '        aLearnMore.style.display = "none";'#13#10+
        '    }'#13#10+
        '    else if (runCount < 101) {'#13#10+
        '        setTimeout(check_aLearnMore, interval);'#13#10+
        '    }'#13#10+
        ' }'#13#10+
        '}');
      if not EMailErr and not PasswordErr then
        Timer1.Enabled := true;
      Application.ProcessMessages;
    end
    else if self.WebBrowser1.LocationURL =
              'https://app.powerbi.com/autoAuthLogin.cshtml?noSignUpCheck=1&ctid=' + Web_Browser.ctid then
    begin
      WebBrowser1.ExecuteScript(
      'window.location.replace("' + Web_Browser.URL + '");');
    end
    else if self.WebBrowser1.LocationURL.StartsWith('https://app.powerbi.com') then
    begin
      WebBrowser1.ExecuteScript(
        'setTimeout(() => {'#13#10+
        ' let mslogo = document.querySelector(".microsoftLogo[_ngcontent-cbo-c431]");'#13#10+
        ' if (mslogo !== null) {'#13#10+
        '  mslogo.style.background = "none";'#13#10+
        ' }'#13#10+
        '}, 100);');

      WebBrowser1.ExecuteScript(
        'setTimeout(() => {'#13#10+
        ' let pbilogo = document.querySelector("#pbi-svg-loading img");'#13#10+
        ' if (pbilogo !== null) {'#13#10+
        '  pbilogo.src = "https://i.ibb.co/jgVF0j6/bi.png";'#13#10+
        '  console.logo("Power Bi logo is found!");'#13#10+
        '  window.chrome.webview.postMessage("Power Bi logo is found!");'#13#10+
        ' }'#13#10+
        ' else {'#13#10+
        '  console.logo("Power Bi logo is not found!");'#13#10+
        '  window.chrome.webview.postMessage("Power Bi logo is not found!");'#13#10+
        ' }'#13#10+
        '}, 100);');

      if self.WebBrowser1.LocationURL.StartsWith('https://app.powerbi.com/groups') or
         self.WebBrowser1.LocationURL.StartsWith('https://app.powerbi.com/view') or
         self.WebBrowser1.LocationURL.StartsWith('https://app.powerbi.com/home') or
         self.WebBrowser1.LocationURL.StartsWith('https://app.powerbi.com/?') then
      else
        AddToLog('Warning', 'Unknown URL:' + self.WebBrowser1.LocationURL);

      if not EMailErr and not PasswordErr then
        Timer1.Enabled := true;
      Application.ProcessMessages;
    end
    else if self.WebBrowser1.LocationURL.StartsWith('https://login.microsoftonline.com/kmsi') then
    else
    begin
      //showMessage('Edge Browser1 location Url:'#13#10+Sender.LocationURL);

      AddToLog('Warning', 'Unknown URL:' + self.WebBrowser1.LocationURL);

      if not EMailErr and not PasswordErr then
        Timer1.Enabled := true;

      Application.ProcessMessages;
    end;

  end;
  //Webbrowser1.DefaultContextMenusEnabled := False;
  //Webbrowser1.DevToolsEnabled := False;
end;

end.
