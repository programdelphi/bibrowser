unit uDm;
interface
uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Stan.StorageBin;
type
  TDm = class(TDataModule)
    mtURL: TFDMemTable;
    dsmtURL: TDataSource;
    mtURLtab_caption: TStringField;
    mtURLtab_url: TStringField;
    mtURLtab_autosize: TIntegerField;
    procedure mtURLtab_autosizeGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
var
  Dm: TDm;
implementation
{%CLASSGROUP 'Vcl.Controls.TControl'}
{$R *.dfm}
procedure TDm.mtURLtab_autosizeGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := EmptyStr;
end;

end.
