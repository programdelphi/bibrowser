unit uAdminPassWord;

interface

uses Winapi.Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Forms,
  Vcl.Controls, Vcl.StdCtrls, Vcl.Buttons;

type
  TfAdminPassword = class(TForm)
    Label1: TLabel;
    eAdminPassWord: TEdit;
    OKBtn: TButton;
    CancelBtn: TButton;
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fAdminPassword: TfAdminPassword;

implementation

{$R *.dfm}

uses uMain, uSetup;

procedure TfAdminPassword.CancelBtnClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfAdminPassword.OKBtnClick(Sender: TObject);
begin
  if eAdminPassWord.Text = fMain.App.Admin_Password then
  begin
    Hide;
    fSetup.ShowModal;
    Application.Terminate;
  end
  else
  begin
    ModalResult := 0;
    eAdminPassWord.Clear;
  end;


end;

end.
