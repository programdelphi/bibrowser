unit uWebBrowser;
interface
uses
  Winapi.Windows, MSHTML, system.SysUtils, system.StrUtils, system.Classes;
type
  TWeb_Browser = class
  private
    FWindow: IHTMLWindow2;
    FDocument: IHTMLDocument2;
    FNewHtmlElement: String;
    FStyleSheet: IHTMLStyleSheet;
    FNewLinkText: String;
    FJavaScript: IHTMLDOMNode;
    FStyleSheetIndex: Integer;
    FURL: String;
    FAutoSize :Boolean;
    FIsIFrame :Boolean;
    function getIsIframe: Boolean;
    function getURL: String;
    function Getctid: string;
  public
    ctid: String;
  published
    procedure ShowBottomBar(pVisible: Bool);
    procedure SetAutoSize(const Value: Boolean);
    procedure SetWindow(const Value: IHTMLWindow2);
    procedure SetDocument(const Value: IHTMLDocument2);
    procedure SetJavaScript(const Value: IHTMLDOMNode);
    procedure SetNewHtmlElement(const Value: String);
    procedure SetNewLinkText(const Value: String);
    procedure SetStyleSheet(const Value: IHTMLStyleSheet);
    procedure SetStyleSheetIndex(const Value: Integer);
    procedure SetURL(const Value: String);
    property Window: IHTMLWindow2 read FWindow write SetWindow;
    property Document: IHTMLDocument2 read FDocument write SetDocument;
    property StyleSheet: IHTMLStyleSheet read FStyleSheet write SetStyleSheet;
    property StyleSheetIndex: Integer read FStyleSheetIndex
      write SetStyleSheetIndex;
    property JavaScript: IHTMLDOMNode read FJavaScript write SetJavaScript;
    property NewHtmlElement: String read FNewHtmlElement
      write SetNewHtmlElement;
    property NewLinkText: String read FNewLinkText write SetNewLinkText;
    property URL: String read getURL write SetURL;
    property AutoSize: Boolean read FAutoSize write SetAutoSize;
    property IsIFrame :Boolean read getIsIframe;
  end;
implementation
{ TWeb_Browser }
function TWeb_Browser.getIsIframe: Boolean;
begin
  Result := (Pos('<IFRAME', UpperCase(FURL))>0) OR (Pos('/IFRAME>', UpperCase(FURL))>0);
end;

function TWeb_Browser.getURL: String;
var indexFile :TStringlist;
    fileName, html, htmlAutoSize :string;
begin
  if Not IsIFrame then
    Result := FURL
  else
  begin
     htmlAutoSize := 'style="width:100%;height:100%" ';
     indexFile := TStringlist.Create;
     try
       html :=
          '<head> ' + chr(13) +
          '<style type="text/css">  ' + chr(13) +
          'html {height:100%}  ' + chr(13) +
          'body { ' + chr(13) +
          'margin:0; ' + chr(13) +
          'height:100%; ' + chr(13) +
          'overflow:hidden ' + chr(13) +
          '} ' + chr(13) +
          '</style> ' + chr(13) +
          '</head> ' + chr(13) +
          '<body> ' + chr(13) +
          ifthen(AutoSize,
            StringReplace(FURL, '<iframe', '<iframe '+htmlAutoSize, [rfReplaceAll, rfIgnoreCase]),
            ' <center>'+ FURL +'</center> ') + chr(13) +
          '</body>' ;
       IndexFile.add(html);
       fileName := ExtractFilePath(paramstr(0)) + 'temp.html';
       IndexFile.savetofile(fileName);
       result := fileName;
     finally
       FreeAndNil(indexFile);
     end;
  end;
end;

Procedure TWeb_Browser.SetAutoSize(const Value: Boolean);
begin
  FAutoSize := Value;
end;

procedure TWeb_Browser.SetDocument(const Value: IHTMLDocument2);
begin
  FDocument := Value;
  SetStyleSheetIndex(Value.StyleSheets.Length);
  SetStyleSheet(Value.CreateStyleSheet('', StyleSheetIndex));
end;
procedure TWeb_Browser.SetJavaScript(const Value: IHTMLDOMNode);
begin
  FJavaScript := Value;
end;
procedure TWeb_Browser.SetNewHtmlElement(const Value: String);
begin
  FNewHtmlElement := Value;
end;
procedure TWeb_Browser.SetNewLinkText(const Value: String);
begin
  FNewLinkText := Value;
end;
procedure TWeb_Browser.SetStyleSheet(const Value: IHTMLStyleSheet);
begin
  FStyleSheet := Value;
end;
procedure TWeb_Browser.SetStyleSheetIndex(const Value: Integer);
begin
  FStyleSheetIndex := Value;
end;
procedure TWeb_Browser.SetURL(const Value: String);
begin
  FURL := Value;

  ctid := Getctid;
end;

function TWeb_Browser.Getctid: string;
var
  posCtidStart: Integer;
  posCtidEd: Integer;
begin
  Result := '';
  posCtidStart := Pos('&ctid=', FURL);
  if posCtidStart > 0 then
  begin
    posCtidStart := posCtidStart + 6;
    posCtidEd := Pos('&', FURL, posCtidStart);
    if posCtidEd > 0 then
      posCtidEd := posCtidEd - 1
    else
      posCtidEd := Length(FURL);
    Result := Copy(FURL, posCtidStart, posCtidEd - posCtidStart + 1);
  end;
end;

procedure TWeb_Browser.SetWindow(const Value: IHTMLWindow2);
begin
  FWindow := Value;
end;
procedure TWeb_Browser.ShowBottomBar(pVisible: Bool);
var
  newHTMLselector: string;
begin
  if pVisible then
  begin
    StyleSheet.cssText := 'logo-bar  { display: none; } ' +
      '.embeddedLandingRootContentLogoVisible { height: 100%; } ' +
      '#pbi-loading > svg { display: none; } ';
    NewHtmlElement :=
      '<img class="pulsing-svg-item" src="http://compudimepos.com/imgs/bi.png">';
    newHTMLselector := '#pbi-loading';
    JavaScript := Document.createElement('script') as IHTMLDOMNode;
    (JavaScript as IHTMLScriptElement).text :=
      'function logo_replacement_fn_13546() { const p = document.querySelector("'
      + newHTMLselector + '"); ' +
      'if(p==null) setTimeout(function() { logo_replacement_fn_13546(); } , 10); else { '
      + ' const newE = document.querySelector("' + newHTMLselector +
      ' > div"); if (newE ==null) { var newItem = document.createElement("div");  p.insertBefore(newItem, p.childNodes[0]);  newItem.innerHTML ='''
      + NewHtmlElement + '''; } ' + ' } }';
    (Document.body as IHTMLDOMNode).appendChild(JavaScript);
    // Execute JS
    Window := Document.parentWindow;
    if Assigned(Window) then
      Window.execScript('logo_replacement_fn_13546()', 'JavaScript');
  end
  else
  begin
    StyleSheet.cssText :=
      '#pbiAppPlaceHolder > ui-view > div > div:nth-child(2) > logo-bar > div > div > div > a { display: none; } '
      +
    // hide Microsoft Power BI
      '#pbiAppPlaceHolder > ui-view > div > div:nth-child(2) > logo-bar > div > div > div > span > logo-bar-social-sharing {display: none;} '
      + // hide sharebar buttons
      '#fullScreenIcon {display: none;} '; // hide full screen button
    // Inject JS (to inject new html)
    NewHtmlElement :=
      '<span _ngcontent-c0="" class="text" localize="Embed_Logobar_MarketingText">'
      + NewLinkText + '</span>';
    JavaScript := Document.createElement('script') as IHTMLDOMNode;
    (JavaScript as IHTMLScriptElement).text :=
      'function logo_replacement_fn_13546() { const p = document.querySelector("#pbiAppPlaceHolder > ui-view > div > div:nth-child(2) > logo-bar > div > div > div"); '
      + 'if(p==null) setTimeout(function() { logo_replacement_fn_13546(); } , 10); else { var newItem = document.createElement("div");  p.insertBefore(newItem, p.childNodes[0]);  newItem.innerHTML ='''
      + NewHtmlElement + '''; } }';
    (Document.body as IHTMLDOMNode).appendChild(JavaScript);
    // Execute JS
    Window := Document.parentWindow;
    if Assigned(Window) then
      Window.execScript('logo_replacement_fn_13546()', 'JavaScript');
  end;
end;
end.
