unit uPassWord;

interface

uses Winapi.Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Forms,
  Vcl.Controls, Vcl.StdCtrls, Vcl.Buttons;

type
  TfPasswordDlg = class(TForm)
    Label1: TLabel;
    ePassword: TEdit;
    OKBtn: TButton;
    CancelBtn: TButton;
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  fPasswordDlg: TfPasswordDlg;

implementation

{$R *.dfm}

uses uMain;

procedure TfPasswordDlg.CancelBtnClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfPasswordDlg.OKBtnClick(Sender: TObject);
begin
  if ePassword.Text = fMain.App.User_Password then
  begin
    ModalResult := mrOk;
  end
  else
  begin
    ModalResult := 0;
    ePassword.Clear;
  end;

end;

end.
