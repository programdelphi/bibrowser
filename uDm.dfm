object Dm: TDm
  OldCreateOrder = False
  Height = 153
  Width = 250
  object mtURL: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvPersistent, rvSilentMode]
    ResourceOptions.Persistent = True
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 31
    Top = 30
    object mtURLtab_caption: TStringField
      DisplayLabel = 'Tab Caption'
      FieldName = 'tab_caption'
      Size = 30
    end
    object mtURLtab_url: TStringField
      DisplayLabel = 'URL ou IFrame'
      DisplayWidth = 250
      FieldName = 'tab_url'
      Size = 1000
    end
    object mtURLtab_autosize: TIntegerField
      DisplayLabel = 'IFrame AutoSize'
      FieldName = 'tab_autosize'
      OnGetText = mtURLtab_autosizeGetText
    end
  end
  object dsmtURL: TDataSource
    DataSet = mtURL
    Left = 96
    Top = 32
  end
end
