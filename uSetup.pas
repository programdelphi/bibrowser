unit uSetup;
interface
uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,     Math  ,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Menus, Vcl.DBCtrls, Vcl.Mask;
type
  TfSetup = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    dtExpDate: TDateTimePicker;
    lbeUserPassword: TLabeledEdit;
    lbeStoreName: TLabeledEdit;
    lbExpDate: TLabel;
    bSaveClose: TButton;
    PopupMenu1: TPopupMenu;
    DeleteRecord1: TMenuItem;
    lbePowerBiUserName: TLabeledEdit;
    lbePowerBIUserPwd: TLabeledEdit;
    procedure FormActivate(Sender: TObject);
    procedure bSaveCloseClick(Sender: TObject);
    procedure DeleteRecord1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fSetup: TfSetup;

  implementation

{$R *.dfm}

uses uDm, uMain;

procedure TfSetup.bSaveCloseClick(Sender: TObject);
begin
  with fMain.App do
  begin
    SetUser_Password(Trim(lbeUserPassword.Text));
    SetStoreName(Trim(lbeStoreName.Text));
    SetExpirationDate(dtExpDate.Date);
    PowerBi_UserName := lbePowerBiUserName.Text;
    PowerBi_UserPassword := lbePowerBIUserPwd.Text;
    SaveSettingsToIni;
    SaveSectionsToIni;
    UpdateSettings(fMain.pHeading);
    UpdateTabSheets(Dm.mtURL, fMain.pcURL);
    Application.Terminate;
  end;
end;
procedure TfSetup.DBGrid1CellClick(Column: TColumn);
begin
   if(UpperCase(Column.FieldName) = uppercase('tab_autosize')) then
   begin
     Dm.dsmtURL.DataSet.Edit;
     if(Dm.dsmtURL.DataSet.FieldByName('tab_autosize').AsInteger = 1) then
     begin
       Dm.dsmtURL.DataSet.FieldByName('tab_autosize').AsInteger := 0;
     end
     else
     begin
       Dm.dsmtURL.DataSet.FieldByName('tab_autosize').AsInteger := 1;
     end;
     Dm.dsmtURL.DataSet.Post;
   end;
end;

procedure TfSetup.DBGrid1ColEnter(Sender: TObject);
begin
  if UpperCase(TDBGrid(Sender).SelectedField.FieldName) = uppercase('tab_autosize') then
    TDBGrid(Sender).Options := TDBGrid(Sender).Options - [dgEditing]
  else
    TDBGrid(Sender).Options := TDBGrid(Sender).Options + [dgEditing];
end;

procedure TfSetup.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
 var
  Check: Integer;
  R: TRect;
begin
  inherited;

  if ((Sender as TDBGrid).DataSource.Dataset.IsEmpty) then
    Exit;

  if(UpperCase(Column.FieldName) = uppercase('tab_autosize')) then
  begin
    TDBGrid(Sender).Canvas.FillRect(Rect);
    if (TDBGrid(Sender).DataSource.DataSet.FieldByName('tab_autosize').AsInteger = 1) then
      Check := DFCS_CHECKED
    else
      Check := 0;
    R := Rect;
    InflateRect(R, -2, -2);
    DrawFrameControl(TDBGrid(Sender).Canvas.Handle, R, DFC_BUTTON,
      DFCS_BUTTONCHECK or Check);
  end;
end;

procedure TfSetup.DeleteRecord1Click(Sender: TObject);
begin
dm.mtURL.Delete;
end;

procedure TfSetup.FormActivate(Sender: TObject);
begin
  with fMain.App do
  begin
    lbeUserPassword.Text := User_Password;
    lbeStoreName.Text := StoreName;
    dtExpDate.Date := ExpirationDate;
    lbePowerBiUserName.Text := PowerBi_UserName;
    lbePowerBIUserPwd.Text := PowerBi_UserPassword;
  end;
end;
procedure TfSetup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Application.Terminate;
end;
end.
