object fSetup: TfSetup
  Left = 0
  Top = 0
  Caption = 'Setup'
  ClientHeight = 341
  ClientWidth = 1262
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnClose = FormClose
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 49
    Width = 1262
    Height = 292
    Align = alClient
    DataSource = Dm.dsmtURL
    PopupMenu = PopupMenu1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    OnColEnter = DBGrid1ColEnter
    OnDrawColumnCell = DBGrid1DrawColumnCell
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1262
    Height = 49
    Align = alTop
    TabOrder = 1
    object lbExpDate: TLabel
      Left = 320
      Top = 3
      Width = 78
      Height = 13
      Caption = 'Expiration Date:'
    end
    object dtExpDate: TDateTimePicker
      Left = 320
      Top = 22
      Width = 186
      Height = 21
      Date = 43726.000000000000000000
      Time = 0.868145821761572700
      TabOrder = 0
    end
    object lbeUserPassword: TLabeledEdit
      Left = 8
      Top = 22
      Width = 121
      Height = 21
      EditLabel.Width = 75
      EditLabel.Height = 13
      EditLabel.Caption = 'User Password:'
      TabOrder = 1
      Text = ''
    end
    object lbeStoreName: TLabeledEdit
      Left = 160
      Top = 22
      Width = 121
      Height = 21
      EditLabel.Width = 60
      EditLabel.Height = 13
      EditLabel.Caption = 'Store Name:'
      TabOrder = 2
      Text = ''
    end
    object bSaveClose: TButton
      Left = 1152
      Top = 1
      Width = 109
      Height = 47
      Align = alRight
      Caption = 'S&ave and Close'
      TabOrder = 3
      OnClick = bSaveCloseClick
    end
    object lbePowerBiUserName: TLabeledEdit
      Left = 545
      Top = 22
      Width = 192
      Height = 21
      EditLabel.Width = 102
      EditLabel.Height = 13
      EditLabel.Caption = 'Power BI User Name:'
      TabOrder = 4
      Text = ''
    end
    object lbePowerBIUserPwd: TLabeledEdit
      Left = 768
      Top = 22
      Width = 128
      Height = 21
      EditLabel.Width = 121
      EditLabel.Height = 13
      EditLabel.Caption = 'Power BI User Password:'
      TabOrder = 5
      Text = ''
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 676
    Top = 184
    object DeleteRecord1: TMenuItem
      Caption = 'Delete Record'
      OnClick = DeleteRecord1Click
    end
  end
end
